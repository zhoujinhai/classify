"""
****************************************************
* Copyright    :    
* Description  :    基于 Pytorch  自定义加载数据
* Project      :    pokemon.py
* Author       :    
* Data         :    2019-8-10
****************************************************
"""

import os
import glob
import csv
import random

from PIL import Image
import torch
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader

class Mydataset(Dataset):
    def __init__(self, root, resize, mode):
        super(Mydataset, self).__init__()
        self.root = root
        self.resize = resize

        self.name2label = {}  # 0,1,2 ...
        for name in sorted(os.listdir(os.path.join(root))):
            if not os.path.isdir(os.path.join(root, name)):
                continue

            self.name2label[name] = len(self.name2label.keys())
        print(self.name2label)
        self.images, self.labels = self.load_csv('imagess.csv')

        if mode == 'train':  # %60 = %0->%60
            self.images = self.images[:int(0.6 * len(self.images))]
            self.labels = self.labels[:int(0.6 * len(self.labels))]
        elif mode == 'val':  # %20 = %60->%80
            self.images = self.images[int(0.6 * len(self.images)):int(0.8 * len(self.images))]
            self.labels = self.labels[int(0.6 * len(self.labels)):int(0.8 * len(self.labels))]
        else:  # %20 = %80->%100
            self.images = self.images[int(0.8 * len(self.images)):]
            self.labels = self.labels[int(0.8 * len(self.labels)):]

    def load_csv(self, filename):
        if not os.path.exists(os.path.join(self.root, filename)):
            images = []
            for name in self.name2label.keys():
                images += glob.glob(os.path.join(self.root, name, '*.png'))
                images += glob.glob(os.path.join(self.root, name, '*.jpg'))
                images += glob.glob(os.path.join(self.root, name, '*.jpeg'))
            print(len(images), images)

            random.shuffle(images)
            with open(os.path.join(self.root, filename), mode='w', newline='') as f:
                write = csv.writer(f)
                for img in images:
                    name = img.split(os.sep)[-2]
                    label = self.name2label[name]
                    write.writerow([img, label])
                print('writen into csv file:', filename)

        # read csv
        images, labels = [], []
        with open(os.path.join(self.root, filename)) as f:
            reader = csv.reader(f)
            for row in reader:
                img, label = row
                label = int(label)

                images.append(img)
                labels.append(label)

        assert len(images) == len(labels)
        return images, labels

    def __len__(self):
        return len(self.images)



    def __getitem__(self, idx):
        # idx-[0->len(images)]
        img, label = self.images[idx], self.labels[idx]
        tf = transforms.Compose([
            lambda x: Image.open(x).convert('RGB'),
            transforms.Resize((int(self.resize * 1.25), int(self.resize * 1.25))),
            transforms.RandomRotation(15),
            transforms.CenterCrop(self.resize),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
        ])

        img = tf(img)
        label = torch.tensor(label)
        return img, label

    def denormalize(self, x_hat):
        mean = [0.485, 0.456, 0.406]
        std = [0.229, 0.224, 0.225]
        # x_hat = (x - mean) / std
        # x = x_hat * std + mean
        # x:[x,h,w]
        # mean: [3] -> [3, 1, 1]

        mean = torch.tensor(mean).unsqueeze(1).unsqueeze(1)
        std = torch.tensor(std).unsqueeze(1).unsqueeze(1)
        x = x_hat * std + mean
        return x


def main():
    import visdom
    import time
    import torchvision

    viz = visdom.Visdom()

    transform = transforms.Compose([
        transforms.Resize((64, 64)),
        transforms.ToTensor()
    ])

    tmp = torchvision.datasets.ImageFolder(root='dataset', transform=transform)
    loader = DataLoader(tmp, batch_size=32, shuffle=True)

    for x, y in loader:
        viz.images(x, nrow=8, win='batch', opts=dict(title='batch'))
        viz.text(str(y.numpy()), win='label', opts=dict(title='batch-y'))
        time.sleep(10)
    """
    # 最初的注释
    tmp = Pokemon('dataset', 64, 'training')
    x, y = next(iter(tmp))
    print('sample:', x.shape, y.shape)
    viz.images(tmp.denormalize(x), win='sample_x', opts=dict(title='sample_x'))
    loader = DataLoader(tmp, batch_size=32, shuffle=True)


    for x, y in loader:
        viz.images(tmp.denormalize(x), nrow=8, win='batch', opts=dict(title='batch'))
        viz.text(str(y.numpy()), win='label', opts=dict(title='batch-y'))
    """


if __name__ == "__main__":
    main()
