
import torch
from PIL import Image
from torchvision import transforms
from torchvision.models.resnet import resnet18
transform=transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485,0.456,0.406],
                                 std=[0.229,0.224,0.225])])




model = resnet18()
model.load_state_dict({k.replace('module.',''):v for k,v in torch.load('resnet18.pkl').items()})


import torch
import torch.onnx
from mmcv import runner

model=runner.load_state_dict({k.replace('module.',' '):v for k,v in state_dict['state_dict'].items()})