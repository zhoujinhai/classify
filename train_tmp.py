import torch
import visdom
import torch.nn as nn
import torch.optim
from mydataset import Mydataset
from torch.utils.data import Dataset, DataLoader
from AlexNet import AlexNet
from GooGLeNet import GoogLeNet
from torchvision.models.googlenet import googlenet

batchsize = 32
learning_rate = 1e-5
epoches = 100
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


train_db = Mydataset('dataset', 32, mode='train')
val_db = Mydataset('dataset', 32, mode='val')
test_db = Mydataset('dataset', 32, mode='test')


train_loader = DataLoader(train_db, batch_size=batchsize, shuffle=True, num_workers=4)
val_loader = DataLoader(val_db, batch_size=batchsize, num_workers=2)
test_loader = DataLoader(test_db, batch_size=batchsize, num_workers=2)

# 构建模型
#model = AlexNet(num_classes=2).to(device)
model = GoogLeNet().to(device)


# 定义损失函数
criterion = nn.CrossEntropyLoss()
# 定义迭代参数的算法
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# 训练模型

viz = visdom.Visdom()

def evaluate(model, loader):
    correct = 0
    total = len(loader.dataset)
    for x, y in loader:
        x, y = x.to(device), y.to(device)
        with torch.no_grad():
            logits = model(x)
            pred = logits.argmax(dim=1)
        correct += torch.eq(pred, y).sum().float().item()
    return correct/total


best_acc = 0
best_epoch = 0
global_step = 0
viz.line([0], [-1], win='loss', opts=dict(title='loss'))
viz.line([0], [-1], win='val_acc', opts=dict(title='val_acc'))
for epoch in range(epoches):
    for step, (x, y) in enumerate(train_loader):
        viz.images(train_db.denormalize(x), nrow=8, win='batch', opts=dict(title='batch'))
        viz.text(str(y.numpy()), win='label', opts=dict(title='batch-y'))

        x, y = x.to(device), y.to(device)
        # x: [batchsize,3,224,224], y:[batchsize]

        logits = model(x)
        loss = criterion(logits, y)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        viz.line([loss.item()], [global_step], win='loss', update='append')
    global_step += 1

    if epoch % 1 == 0:
        val_acc = evaluate(model, val_loader)
        if val_acc > best_acc:
            best_epoch = epoch
            best_acc = val_acc
            torch.save(model.state_dict(), 'best.mdl')
            viz.line([val_acc], [global_step], win='val_acc', update='append')

print("best acc:", best_acc, "best epoch:", best_epoch)
model.load_state_dict(torch.load("best.mdl"))
print("loaded from ckpt!")

test_acc = evaluate(model, test_loader)
print("test acc:", test_acc)











